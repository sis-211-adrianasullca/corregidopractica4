package herencia;

public class coche extends Vehiculo{
	private String marca;
    private String modelo;

    public coche(String marca, String modelo) {
        super();
        this.marca = marca;
        this.modelo = modelo;
    }

    public void cambiarModelo(String nuevoModelo) {
        modelo = nuevoModelo;
        System.out.println("El modelo del coche se ha cambiado a: " + nuevoModelo);
    }

    public void quemarRueda() {
        System.out.println("¡Has quemado rueda con el coche!");
    }

}
