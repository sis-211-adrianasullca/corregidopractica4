package herencia;

public class Bicicleta extends Vehiculo{
	 private int velocidad;

	    public Bicicleta() {
	        super();
	        velocidad = 0;
	    }

	    public void aumentarVelocidad(int incremento) {
	        velocidad += incremento;
	        System.out.println("La velocidad de la bicicleta se ha aumentado en " + incremento + " km/h.");
	    }

	    public void hacerCaballito() {
	        System.out.println("¡Has hecho el caballito con la bicicleta!");
	    }

}
