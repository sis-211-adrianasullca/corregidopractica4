package herencia;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
        Bicicleta bicicleta = new Bicicleta();
        coche coche = new coche("Ford", "Mustang");
        

        int opcion;

        do {
            System.out.println("----- MENÚ -----");
            System.out.println("1. Anda con la bicicleta");
            System.out.println("2. Haz el caballito con la bicicleta");
            System.out.println("3. Anda con el coche");
            System.out.println("4. Quema rueda con el coche");
            System.out.println("5. Ver kilometraje de la bicicleta");
            System.out.println("6. Ver kilometraje del coche");
            System.out.println("7. Ver kilometraje total");
            System.out.println("8. Salir");
            System.out.println("Elige una opción (1-8):");
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    bicicleta.recorrer(10);
                    break;
                case 2:
                    bicicleta.hacerCaballito();
                    break;
                case 3:
                    coche.recorrer(20);
                    break;
                case 4:
                    coche.quemarRueda();
                    break;
                case 5:
                    System.out.println("Kilometraje de la bicicleta: " + bicicleta.kilómetrosRecorridos);
                    break;
                case 6:
                    System.out.println("Kilometraje del coche: " + coche.kilómetrosRecorridos);
                    break;
                case 7:
                    System.out.println("Kilometraje total: " + Vehiculo.kilometrosTotales);
                    break;
                case 8:
                    System.out.println("¡Hasta luego!");
                    break;
                default:
                    System.out.println("Opción inválida. Intente nuevamente.");
                    break;
            }
        } while (opcion != 8);

	}

}
