package herencia;

public class Vehiculo {
	protected static int vehículosCreados;
    protected static int kilometrosTotales;
    protected int kilómetrosRecorridos;

    public Vehiculo() {
        vehículosCreados++;
    }

    public void recorrer(int kilómetros) {
        kilómetrosRecorridos += kilómetros;
        kilometrosTotales += kilómetros;
        System.out.println("Se han recorrido " + kilómetros + " kilómetros.");
    }

    public void mostrarInformacion() {
        System.out.println("Vehículos creados: " + vehículosCreados);
        System.out.println("Kilómetros totales: " + kilometrosTotales);
        System.out.println("Kilómetros recorridos: " + kilómetrosRecorridos);
    }

}
