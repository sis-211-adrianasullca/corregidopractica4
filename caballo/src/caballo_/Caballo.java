package caballo_;

public class Caballo {
	private String nombre;
    private int edad;
    private String nacionalidad;
    private String equipo;
    private int numeroCarrera;
    private int puntos;
    private int experiencia;
    private String estadisticas;
    private double salario;

    public Caballo(String nombre, int edad, String nacionalidad, String equipo, int numeroCarrera, int puntos, int experiencia, String estadisticas, double salario) {
        this.nombre = nombre;
        this.edad = edad;
        this.nacionalidad = nacionalidad;
        this.equipo = equipo;
        this.numeroCarrera = numeroCarrera;
        this.puntos = puntos;
        this.experiencia = experiencia;
        this.estadisticas = estadisticas;
        this.salario = salario;
    }

    public void mostrarInformacion() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Edad: " + edad);
        System.out.println("Nacionalidad: " + nacionalidad);
        System.out.println("Equipo: " + equipo);
        System.out.println("Número de carrera: " + numeroCarrera);
        System.out.println("Puntos: " + puntos);
        System.out.println("Experiencia: " + experiencia + " años");
        System.out.println("Estadísticas: " + estadisticas);
        System.out.println("Salario: " + salario);
    }

}
